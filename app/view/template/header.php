<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php if ($view == "home"): ?>
    <link rel="canonical" href="<?php echo URL; ?>" />
	<?php endif; ?>
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="hdLeft">
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home</a></li>
							<li id="dropdown"><a href="#">Services</a>
								<ul class="dropdown-content">
									<li <?php $this->helpers->isActiveMenu("pest-control-services"); ?>><a href="<?php echo URL ?>pest-control-services">Pest Control Services</a></li>
									<li <?php $this->helpers->isActiveMenu("animal-removal"); ?>><a href="<?php echo URL ?>animal-removal">Animal Removal</a></li>
									<li <?php $this->helpers->isActiveMenu("remodeling-services"); ?>><a href="<?php echo URL ?>remodeling-services">Remodeling Services</a></li>
									<li <?php $this->helpers->isActiveMenu("home-additions"); ?>><a href="<?php echo URL ?>home-additions">Home Additions</a></li>
								</ul>
							</li>
							<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">Gallery</a></li>
							<li <?php $this->helpers->isActiveMenu("testimonials"); ?>><a href="<?php echo URL ?>testimonials">Testimonials</a></li>
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">Contact Us</a></li>
						</ul>
					</nav>
				</div>
				<div class="hdRight">
					<?php $this->info(["phone","tel","hdPhone"]); ?>
				</div>
				<div class="hdLogo">
					<a href="<?php echo URL; ?>">
						<img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name"); ?> Main Logo"/>
					</a>
				</div>
			</div>
		</div>
	</header>

	<?php if($view == "home"):?>
		<div id="banner">
			<div class="banner-img">
				<img src="public/images/common/bannerBG.jpg" alt="Banner BG">
			</div>
			<div class="row">
				<h2>Premier Pest Control Service</h2>
				<p>IN KING GEORGE/NORTHERN VIRGINIA.</p>
				<a href="contact#content" class="btn">ASK ABOUT OUR SPECIAL DISCOUNT</a>
			</div>
		</div>
	<?php endif; ?>
