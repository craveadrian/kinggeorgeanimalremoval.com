<div id="content">
  <div class="row">
    <img src="public/images/content/inner-service2.jpg" alt="Animal Removal">
    <h1>Animal Removal</h1>
    <p>If you have an animal living in your attic, crawlspace, etc., we are happy to help get it out with our animal removal services. Whether it's dealing with raccoons or birds, we have all equipment to get them off your property safely. If you need more information about our animal removal services, contact Hilton Home And Wildlife Services today. </p>
    <a href="contact#content" class="btn">Contact Us Today!</a>
  </div>
</div>
