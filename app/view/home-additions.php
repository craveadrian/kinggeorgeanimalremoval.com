<div id="content">
  <div class="row">
    <img src="public/images/content/inner-service4.jpg" alt="Home Additions">
    <h1>Home Additions</h1>
    <p>At Hilton Home And Wildlife Services, we build all types of home additions, such as building deck, porches, siding, and more. Just let us know what you need and we'll do our best to get it done fast and correctly. For more information about our home remodeling services, contact Hilton Home And Wildlife Services.</p>
    <a href="contact#content" class="btn">Contact Us Today!</a>
  </div>
</div>
