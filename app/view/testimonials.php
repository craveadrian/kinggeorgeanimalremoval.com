<div id="content">
  <div class="row">
    <h1>Testimonials</h1>
    <div class="testimonial">
      <p class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
      <p>
        <span>
          Comment:
        </span>
          Hilton Home And Wildlife Services is rated 5.0 out of 5 stars, based on 5 reviews!
      </p>
    </div>
    <div class="testimonial">
      <p class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
      <p>
        <span>
          Comment:
        </span>
          The owner is very polite and professional.
      </p>
    </div>
    <div class="testimonial">
      <p class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
      <p>
        <span>
          Comment:
        </span>
          The owner is very polite and professional.
      </p>
    </div>
    <div class="testimonial">
      <p class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
      <p>
        <span>
          Comment:
        </span>
          .
      </p>
    </div>
    <div class="testimonial">
      <p class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
      <p>
        <span>
          Comment:
        </span>
        Called and set up a consult the same day. <br/>
        Dwayne is up front with all plans and prices. <br/>
        Came to our house daily to check traps and kept constant contact with us. The end of our job required some dirt work. We were happy with Dwayne's work and cleaning up after the work. <br/>
        I will recommend Hilton Home and Wildlife Services in the future.
      </p>
    </div>
    <div class="testimonial">
      <p class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
      <p>
        <span>
          Comment:
        </span>
        Very professional and upfront with the pricing during the inspection, and kept me informed the process.
      </p>
    </div>
  </div>
</div>
