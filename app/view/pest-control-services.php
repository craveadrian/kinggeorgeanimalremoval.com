<div id="content">
  <div class="row">
    <img src="public/images/content/inner-service1.jpg" alt="Pest Control Services">
    <h1>Pest Control Services</h1>
    <p>Pests can produce severe damage to your property without even being noticed, which makes extremely important to handle extermination immediately when they are discovered. We offer exterminator services for pests and termites, handling the job with expedience and efficiency. To take advantage of our exterminator services, please contact us at Hilton Home And Wildlife Services, today.</p>
    <a href="contact#content" class="btn">Contact Us Today!</a>
  </div>
</div>
