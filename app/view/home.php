<div id="services">
	<div class="row mobile-space-around">
		<dl>
			<dt><img src="public/images/content/service1.jpg" alt="Pest Control" /></dt>
			<dd>
				<h4>Pest Control</h4>
				<p>Bug Extermination</p>
			</dd>
		</dl>
		<dl>
			<dt><img src="public/images/content/service2.jpg" alt="Animal Removal" /></dt>
			<dd>
				<h4>Animal Removal</h4>
				<p>Raccoon Removal</p>
				<p>Bird Control Service</p>
			</dd>
		</dl>
		<dl>
			<dt><img src="public/images/content/service3.jpg" alt="Home Remodeling" /></dt>
			<dd>
				<h4>Home Remodeling</h4>
				<p>Kitchen Remodeling</p>
				<p>Siding</p>
			</dd>
		</dl>
	</div>
</div>
<div id="welcome">
	<div class="row">
		<img src="public/images/content/rat.jpg" alt="Rat" class="wlc-rat">
		<div class="wlc-text">
			<h1>Hilton Home</h1>
			<h3>AND WILDLIFE SERVICES</h3>
			<p>Welcome to Hilton Home And Wildlife Services, the premier pest control service in King George, VA and the surrounding areas. If you are having problems with pest or animals living in your house, we have all the equipment to get them out safely and efficiently. While pest control is our main services, we also provide home remodeling and home addition services.</p>
			<div class="buttons">
				<a href="services#content" class="btn">READ MORE</a>
				<a href="contact#content" class="btn2">FREE ESTIMATE</a>
			</div>
			<?php $this->info(["phone","tel","wlcPhone"]); ?>
		</div>
	</div>
</div>
<div id="about">
	<div class="row">
		<div class="text">
			<h2>About Us</h2>
			<p>Hilton Home And Wildlife Services  is a team of highly skilled individuals, motivated and devoted to satisfying all your home needs. Whether it be home improvement repairs or insect and animal removal, we have what it takes to get the job done! With over 30 years of experience, we pride ourselves on quality, not quantity. Our excellent customer service and affordable prices are guaranteed to please all of our customers.</p>
			<p>For more information about any of our services, contact Hilton Home And Wildlife Services today!</p>
			<a href="about#content" class="btn2">READ MORE</a>
		</div>
		<img src="public/images/content/about-img.jpg" alt="ceiling">
	</div>
</div>
<div id="reviews">
	<div class="row">
		<h2>What <span>They Say</span></h2>
		<div class="container">
			<div class="review-box">
				<p class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
				<p class="text">Called and set up a consult the same day. Dwayne is up front with all plans and prices. Came to our house daily to check traps and kept constant contact with us. We were happy with Dwayne’s work and cleaning up after the work. </p>
			</div>
			<div class="review-box">
				<p class="star">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
				<p class="text">Very professional and upfront with the pricing during the inspection, and kept me informed the process.</p>
			</div>
			<a href="testimonials#content" class="btn">READ MORE</a>
		</div>
	</div>
</div>
<div id="recent-project">
	<div class="row">
		<div class="rp-left">
			<img src="public/images/content/project1.jpg" alt="Project 1">
			<div class="header">
				<h2>Recent Projects</h2>
			</div>
			<img src="public/images/content/project2.jpg" alt="Project 2">
			<img src="public/images/content/project3.jpg" alt="Project 3">
		</div>
		<img src="public/images/content/project4.jpg" alt="Project 4" class="rp-right">
	</div>
</div>
<div id="quote">
	<div class="row">
		<p>You can always rely on us for quick solutions to unsightly problems.</p>
		<?php $this->info(["phone","tel","qPhone"]); ?>
	</div>
</div>
<div id="qContact">
	<div class="row">
		<h2>Contact Us</h2>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<div class="qform-top">
				<label><span class="ctc-hide">Name</span>
					<input type="text" name="name" placeholder="Name:">
				</label>
				<label><span class="ctc-hide">Phone</span>
					<input type="text" name="phone" placeholder="Phone:">
				</label>
				<label><span class="ctc-hide">Email</span>
					<input type="text" name="email" placeholder="Email:">
				</label>
			</div>
			<label><span class="ctc-hide">Message</span>
				<textarea name="message" cols="30" rows="10" placeholder="Message/Questions:"></textarea>
			</label>
			<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
			<div class="g-recaptcha"></div>
			<div class="qform-bottom">
				<label>
					<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
				</label><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
				</label>
				<?php endif ?>
			</div>
			<button type="submit" class="ctcBtn btn2" disabled>SUBMIT FORM</button>
		</form>
		<div class="container">
			<div class="info-email">
				<?php $this->info(["email","mailto","email"]); ?>
			</div>
			<div class="info-phone">
				<?php $this->info(["phone","tel","phone"]); ?>
			</div>
			<div class="info-address">
				<a href="#" target="_blank" class="address"><?php $this->info("address"); ?></a>
			</div>
		</div>
	</div>
</div>
